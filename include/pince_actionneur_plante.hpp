#include <Arduino.h>

#include "Servo.h"
#include "core/unit.hpp"

enum HeightPince {
    TAKE,
    DROP,
    KEEP,
};
enum ServoSpeed {
    AS_FAST_AS_POSSIBLE,
    SLOW,
};

class PinceActionneurPlante {
   protected:
    Servo servo_height, servo_right, servo_left;
    struct OppeningAngles { int left, right; };
    int target_angle_height; OppeningAngles target_angle_openning;

   public:
    PinceActionneurPlante(const pin pwm_servo_height, const pin pwm_servo_right, const pin pwm_servo_left);

    void go_to_height(HeightPince height_pince, ServoSpeed speed = AS_FAST_AS_POSSIBLE);
    void go_to_openning(bool open, ServoSpeed speed);
    void open(ServoSpeed speed = AS_FAST_AS_POSSIBLE);
    void close(ServoSpeed speed = AS_FAST_AS_POSSIBLE);

    void update();

    bool is_target_reached();

   protected:
    int get_angle_for_height(HeightPince height_pince);
    OppeningAngles get_angles_for_openning(bool open);
};
