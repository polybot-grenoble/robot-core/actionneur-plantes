#include <Servo.h>

#include <chrono>
#include <iostream>
#include <thread>

#include "core/core.hpp"
#include "pince_actionneur_plante.hpp"

enum CommandActionneurPlante {
    OPEN_FAST = 1,
    CLOSE_FAST = 2,
    HEIGHT_KEEP_FAST = 3,
    HEIGHT_TAKE_FAST = 4,
    KEEP_OPEN_FAST = 5,
    KEEP_CLOSE_FAST = 6,
    HEIGHT_DROP_FAST = 7,
    OPEN_SLOW = 8,
    CLOSE_SLOW = 9,
    HEIGHT_KEEP_SLOW = 10,
    HEIGHT_TAKE_SLOW = 11,
    HEIGHT_DROP_SLOW = 12,
};

PinceActionneurPlante *pince_actionneur_plante;

void Talos_initialisation() {
    /**
     * Code to be run on startup and when exiting error state.
     * Make sure you don't init your pins twice.
     */
    pince_actionneur_plante = new PinceActionneurPlante(A5, A0, A2);
    Core_setMinimumLoopPeriod((micro_second) 15000);
    Serial.println("Init end");
}

void Talos_loop() {
    /**
     * Looping code. Stopped when in error state.
     */
    pince_actionneur_plante->update();
}

void Talos_onError() {
    /**
     * Looping code when in error state.
     */
    Serial.println("Thalos error");
    // Core_log("Problème");
    Core_softResetIfConnected();
    pince_actionneur_plante->go_to_height(HeightPince::KEEP);
    pince_actionneur_plante->open();
}

void Core_priorityLoop() {
    /**
     * Looping code no matter what the current state is.
     */
    // Serial.println("thalos prio");
}

void Hermes_onMessage(Hermes_t *hermesInstance, uint8_t sender,
                      uint16_t command, uint32_t isResponse) {
    /**
     * Code to be run every time a message is received over the CAN network.
     */
    // Serial.printf("%d\n", command);

    switch ((CommandActionneurPlante) command) {
        case OPEN_FAST: /*ouverture pince*/
            pince_actionneur_plante->open();
            Serial.println("Ouvrir");
            break;
        case CLOSE_FAST: /*fermeture pince*/
            pince_actionneur_plante->close();
            Serial.println("Fermer");
            break;
        case HEIGHT_KEEP_FAST:
            pince_actionneur_plante->go_to_height(HeightPince::KEEP);
            Serial.println("Monter");
            break;
        case HEIGHT_TAKE_FAST:
            pince_actionneur_plante->go_to_height(HeightPince::TAKE);
            Serial.println("Descendre");
            break;
        case KEEP_OPEN_FAST:
            pince_actionneur_plante->go_to_height(HeightPince::KEEP);
            pince_actionneur_plante->open();
            Serial.println("Stop");
            break;
        case KEEP_CLOSE_FAST:
            pince_actionneur_plante->go_to_height(HeightPince::KEEP);
            pince_actionneur_plante->close();
            break;
        case HEIGHT_DROP_FAST:
            pince_actionneur_plante->go_to_height(HeightPince::DROP);
            break;
        case OPEN_SLOW:
            pince_actionneur_plante->open(ServoSpeed::SLOW);
            break;
        case CLOSE_SLOW:
            pince_actionneur_plante->close(ServoSpeed::SLOW);
            break;
        case HEIGHT_KEEP_SLOW:
            pince_actionneur_plante->go_to_height(HeightPince::KEEP, ServoSpeed::SLOW);
            break;
        case HEIGHT_TAKE_SLOW:
            pince_actionneur_plante->go_to_height(HeightPince::TAKE, ServoSpeed::SLOW);
            break;
        case HEIGHT_DROP_SLOW:
            pince_actionneur_plante->go_to_height(HeightPince::DROP, ServoSpeed::SLOW);
            break;
    }
}
