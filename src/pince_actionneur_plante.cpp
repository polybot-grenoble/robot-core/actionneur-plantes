#include "pince_actionneur_plante.hpp"

PinceActionneurPlante::PinceActionneurPlante(const pin pwm_servo_height, const pin pwm_servo_right, const pin pwm_servo_left) {
    servo_height.attach(pwm_servo_height);
    servo_left.attach(pwm_servo_left);
    servo_right.attach(pwm_servo_right);
    go_to_height(KEEP);
    close();
}

int PinceActionneurPlante::get_angle_for_height(HeightPince height_pince) {
    switch (height_pince) {
        case HeightPince::TAKE:
            return 137;
        case HeightPince::DROP:
            return 110;
        case HeightPince::KEEP:
            return 70;
    }
}

void PinceActionneurPlante::go_to_height(HeightPince height_pince, ServoSpeed speed) {
    int current_angle = servo_height.read();
    target_angle_height = get_angle_for_height(height_pince);
    switch (speed) {
        case AS_FAST_AS_POSSIBLE:
            servo_height.write(target_angle_height);
            break;
    }
}

void PinceActionneurPlante::open(ServoSpeed speed) {
    go_to_openning(true, speed);
}
void PinceActionneurPlante::close(ServoSpeed speed) {
    go_to_openning(false, speed);
}

PinceActionneurPlante::OppeningAngles PinceActionneurPlante::get_angles_for_openning(bool open) {
    switch (open) {
        case true:
            return OppeningAngles{left : 150, right : 30};
        case false:
            return OppeningAngles{left : 86, right : 94};
    }
}

void PinceActionneurPlante::go_to_openning(bool open, ServoSpeed speed) {
    target_angle_openning = get_angles_for_openning(open);
    switch (speed) {
        case AS_FAST_AS_POSSIBLE:
            servo_left.write(target_angle_openning.left);
            servo_right.write(target_angle_openning.right);
            break;
    }
}

void PinceActionneurPlante::update() {
    // Command height servo
    const int current_height_angle = servo_height.read();
    if (current_height_angle < target_angle_height)
        servo_height.write(current_height_angle + 1);
    else if (current_height_angle > target_angle_height)
        servo_height.write(current_height_angle - 1);

    const int current_left_angle = servo_left.read();
    // Command left servo
    if (current_left_angle < target_angle_openning.left)
        servo_left.write(current_left_angle + 1);
    else if (current_left_angle > target_angle_openning.left)
        servo_left.write(current_left_angle - 1);

    const int current_right_angle = servo_right.read();
    // Command right servo
    if (current_right_angle < target_angle_openning.right)
        servo_right.write(current_right_angle + 1);
    else if (current_right_angle > target_angle_openning.right)
        servo_right.write(current_right_angle - 1);
}

bool PinceActionneurPlante::is_target_reached() {
    return target_angle_height == servo_height.read() && target_angle_openning.left == servo_left.read() && target_angle_openning.right == servo_right.read();
}
