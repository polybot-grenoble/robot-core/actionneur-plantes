# Actionneir plante

| Command | command id |
|---------------|------|
| OPEN_FAST | 1 |
| CLOSE_FAST | 2 |
| HEIGHT_KEEP_FAST | 3 |
| HEIGHT_TAKE_FAST | 4 |
| KEEP_OPEN_FAST | 5 |
| KEEP_CLOSE_FAST | 6 |
| HEIGHT_DROP_FAST | 7 |
| OPEN_SLOW | 8 |
| CLOSE_SLOW | 9 |
| HEIGHT_KEEP_SLOW | 10 |
| HEIGHT_TAKE_SLOW | 11 |
| HEIGHT_DROP_SLOW | 12 |
